-- message
create table message (
    id            int,
    theme_id      int,
    user_id       int,
    parent_id     int, -- у тредов != NULL
    text          text,
    total_likes   integer default 0,
    total_dislikes integer default 0,
    created_at    TIMESTAMP,
    updated_at    TIMESTAMP,
    created_by    varchar(255), -- id модератора/юзера
    updated_by    varchar(255) -- id модератора/юзера если что-то измеилось
);

-- это draft, будем делать когда дадут файловую систему
create table attachment (
    id int,
    message_id int,
    link varchar(255)
);

-- это draft, у них вроде конфлюенс, мб просто линки будем хранить
create table wiki (
    id int,
    link varchar(255)
);

create table event_log (
    id int,
    user_consumer_id int, -- кому поставили
    user_producer_id int, -- кто поставил
    created_at TIMESTAMP,
    event_type int, -- например это либо лайк=1, либо дислайк=2, изменение сообщения=3,
                    -- удаление сообщения=4, либо не знаю что
                    -- мейби енумчик
    event_info varchar(255) -- в ней будет json'чик с например message_id, theme_id,
                            -- то есть над чем он взаимодействовал
);
    -- когда был поставлен - зачем? - чтобы была возможность не кидать чувакам бабос повторно,
    -- т.е. раз в месяц/полгода собираем стату, а прошлую стату (лайки) не должны учитывать

-- TODO: возможно табличка со статистикой, будут например count'ы лайков отдаваться в профиль/домашнюю страничку
-- проблемы: возможная неконсистентность, однако не так важна эта инфа, мб пересчитывать время от времени
-- плюсы: не нужно лезть в таблички, не задудосят с этой инфой

-- section - объединили с theme
-- theme
create table theme
(
    id           INT,
    name         char(50),
    parent_id    INT, -- parent_id если NULL - то раздел, иначе тема раздела
    created_at   TIMESTAMP,
    updated_at   TIMESTAMP,
    created_by   varchar(255), -- id модератора/юзера
    updated_by   varchar(255) -- id модератора/юзера если что-то измеилось
);


-- subscribe
create table subscribe (
    id            INT,
    user_id       INT,
    theme_id      INT
);

-- user -- если что поделим на табличку с доп инфой по юзеру -- пока так живем, все в одном месте
create table users (
    id           serial not null constraint users_pk primary key,
    email        key, -- чтобы чувак не регал невалидные почты (@gmail.com, @ya.ru)
                      -- просто регулярку вешаем, валидные только @onegroup.ru
    -- bitrix_token varchar(5000), -- это если мы не сможем разрулить почту
    login        varchar(255),
    role_id      INT,
    user_meta_id int,
    created_at   TIMESTAMP,
    updated_at   TIMESTAMP, -- если роль обновили
    updated_by   varchar(255), -- кем например была сменена роль у юзера
    level int -- эта тема для рассчета весовых коэффициентов при выдаче бабок (можем сделать experience)
);

create table profile_info (
    user_id int,
    full_name    varchar(255), -- увеличить, либо перенесем в другую табличку -- скорее всего поделим на first_name и т.д.
    about_user   varchar(255), -- графа "О СЕБЕ", увеличить, либо перенесем в другую табличку
    profile_photo varchar(255), -- тут будет линка на картинку в файловой системе (лучше svg, а не png/jpeg)
    skills varchar(255), -- увеличить + enum где-то хранить, чтобы могли выбирать (?)
    total_rating int, -- мб в users перенести
    total_messages int -- и так далее с темами, с ответами, то есть чтобы в моменте не собирать эту инфу
    -- докинуть потом при необходимости еще полей
);

-- right
create table right (
    id          INT,
    name        varchar(50),
    description varchar(255),
    created_at  TIMESTAMP,
    updated_at  TIMESTAMP
);
-- role
create table role (
    id          INT,
    name        varchar(50),
    description varchar(255),
    created_at  TIMESTAMP,
    updated_at  TIMESTAMP
);
-- role_right_relation
create table role_right_relation (
    id INT, -- он не нужен, как и ты
    role_id INT,
    right_id INT
);

--
-- 1 раз в месяц выгрузку рейтингов, чтобы в том или ином виде была историчность
-- + активность пользаков тут же в моменте показывает
-- + в течение месяца рейтинг туда сюда идет, мы раз в месяц обнуляем
-- - если чувак сморозил что-то нехорошее - как считать дизлайки (весовой коэффициент вводим), надо учесть canceling людей - наподумать
-- предваритлеьно (обсудить) берем сообщенки с датой скажем в < чем 30 дней (собираем стату по такому принципу)
-- т.е. оставил сообщенку, сколько собралось за 30 дней, за столько и заплатим
-- (обсудить) нужно все-таки следить кто ставит лайкосы - а зачем? - минусы жрет кучу места
--
-- 1. чувак отправил сообщение
-- 2. получил за него лайки
-- 3. мы нагеренерили рейтинг
-- 4. раз в месяц сделали стату, обнулили рейтинг свой (не его прям в профиле)
-- 5. выплитили бабки



-- create view messages_into_theme(count, theme_id) as
-- SELECT (SELECT COALESCE(count(m.*), 0::bigint) AS count) AS count,
--        t.id                                              AS theme_id
-- FROM themes t
--          LEFT JOIN messages m ON m.theme_id = t.id
-- GROUP BY t.id;
--
-- create view comments_into_message(count, message_id) as
-- SELECT (SELECT COALESCE(count(c.*), 0::bigint) AS count) AS count,
--        m.id                                              AS message_id
-- FROM messages m
--          LEFT JOIN comments c ON m.id = c.message_id
-- GROUP BY m.id;
--
-- create view user_stats(id, likes, dislikes) as
-- SELECT users.id,
--        ((SELECT COALESCE(sum(t.likes), 0::bigint) AS "coalesce")) +
--        ((SELECT COALESCE(sum(m.likes), 0::bigint) AS "coalesce"))    AS likes,
--        ((SELECT COALESCE(sum(t.dislikes), 0::bigint) AS "coalesce")) +
--        ((SELECT COALESCE(sum(m.dislikes), 0::bigint) AS "coalesce")) AS dislikes
-- FROM users
--          LEFT JOIN messages m ON users.id = m.user_id
--          LEFT JOIN themes t ON users.id = t.user_id
-- GROUP BY users.id;

